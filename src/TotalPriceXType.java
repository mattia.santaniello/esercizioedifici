import java.math.BigDecimal;

public class TotalPriceXType implements Comparable<TotalPriceXType>{
    private String tipoEdificio;
    private BigDecimal price;

    public TotalPriceXType(String tipoEdificio,BigDecimal price){
        this.tipoEdificio=tipoEdificio;
        this.price=price;
    }

    public void setTipoEdificio(String tipoEdificio){this.tipoEdificio=tipoEdificio;}
    public void setPrice(BigDecimal price){this.price=price;}

    public String getTipoEdificio(){return this.tipoEdificio;}
    public BigDecimal getPrice(){return this.price;}

    public int compareTo(TotalPriceXType t){ return price.compareTo(t.getPrice());}

    public void add(BigDecimal b){
        price=price.add(b);
    }

    public boolean equals(TotalPriceXType t){
        return tipoEdificio.matches(t.getTipoEdificio()) && price.doubleValue() == t.getPrice().doubleValue();
    }

    @Override
    public String toString(){
        return "\ntipo di edificio:"+tipoEdificio+"\nprice:"+price.doubleValue()+"\n";
    }
}
