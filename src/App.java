import java.math.BigDecimal;
public class App {
    public static void main(String[] args) throws Exception {
        GestionePatrimoniale g = new GestionePatrimoniale();

        g.loadCSV("Sacramentorealestatetransactions.csv");
        g.sort();
        
        g.remove(new Residential("9401 BARREL RACER CT","WILTON",95693,"CA",4,3,4400,"Residential","Fri May 16 00:00:00 EDT 2008",new BigDecimal(884790),38.415298,-121.194858));
        g.save("backup.dat");
        
        GestionePatrimoniale g1 = new GestionePatrimoniale();
        g1.load("backup.dat");
        g1.add(new Condo("9401 BARREL RACER CT","WILTON",95693,"CA",4,3,4400,"Residential","Fri May 16 00:00:00 EDT 2008",new BigDecimal(10000000),38.415298,-121.194858));
        g1.stampaElenco();

        for(TotalPriceXType i: g1.totalPrice()){
            System.out.println(i.toString());
        }
    }
}
