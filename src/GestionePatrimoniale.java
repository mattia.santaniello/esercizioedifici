import java.util.ArrayList;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.math.BigDecimal;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.File;
import java.util.Collections;
import java.util.Scanner;
import java.io.IOException;

public class GestionePatrimoniale {
    private ArrayList<Edificio> elencoPatrimoniale; 

    public GestionePatrimoniale(){ this.elencoPatrimoniale = new ArrayList<Edificio>();}

    public void add(Edificio e){
        elencoPatrimoniale.add(e);
    }

    public void save(String fileName){
        FileOutputStream fOut;
        ObjectOutputStream out;
        try{
            fOut = new FileOutputStream(new File(fileName));
            out = new ObjectOutputStream(fOut);

            out.writeObject(elencoPatrimoniale);
            out.close();
            fOut.close();
            System.out.println("il salvataggio nel file "+fileName+" è stato completato");
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    public void load(String fileName){
        File f = new File(fileName);

        if(f.exists()){
            try{
                FileInputStream fOut = new FileInputStream(new File(fileName));
                ObjectInputStream out = new ObjectInputStream(fOut);

                elencoPatrimoniale=(ArrayList<Edificio>)out.readObject();
                out.close();
                fOut.close();
                System.out.println("caricamento dal file "+fileName+" completato");
            }catch(Exception e){
                e.printStackTrace();
            }
        }else
            System.out.println("il file non è stato trovato");
    }

    public void loadCSV(final String nomefile) throws IOException {
        Scanner scanner = new Scanner(new File(nomefile)).useDelimiter(System.lineSeparator());
        try {
            while (scanner.hasNext()) {
                final String[] valori = scanner.next().split(";");

                switch(valori[7]){
                    case "Condo":
                        elencoPatrimoniale.add(new Condo(valori[0],valori[1],Integer.valueOf(valori[2]),valori[3],Integer.valueOf(valori[4]),Integer.valueOf(valori[5]),
                                                Integer.valueOf(valori[6]),valori[7],valori[8],new BigDecimal(valori[9]),Double.valueOf(valori[10]),Double.valueOf(valori[11])));
                        break;
                    case "Multi-Family":
                        elencoPatrimoniale.add(new MultiFamily(valori[0],valori[1],Integer.valueOf(valori[2]),valori[3],Integer.valueOf(valori[4]),Integer.valueOf(valori[5]),
                                                Integer.valueOf(valori[6]),valori[7],valori[8],new BigDecimal(valori[9]),Double.valueOf(valori[10]),Double.valueOf(valori[11])));
                        break;
                    case "Residential":
                        elencoPatrimoniale.add(new Residential(valori[0],valori[1],Integer.valueOf(valori[2]),valori[3],Integer.valueOf(valori[4]),Integer.valueOf(valori[5]),
                                                Integer.valueOf(valori[6]),valori[7],valori[8],new BigDecimal(valori[9]),Double.valueOf(valori[10]),Double.valueOf(valori[11])));
                        break;
                    case "Unknown":
                        elencoPatrimoniale.add(new Unknown(valori[0],valori[1],Integer.valueOf(valori[2]),valori[3],Integer.valueOf(valori[4]),Integer.valueOf(valori[5]),
                                                Integer.valueOf(valori[6]),valori[7],valori[8],new BigDecimal(valori[9]),Double.valueOf(valori[10]),Double.valueOf(valori[11])));
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally{
            scanner.close();
        }
    }

    public void stampaElenco(){
        for(Edificio i:elencoPatrimoniale){
            System.out.println(i.toString());
        }
    }

    public ArrayList<TotalPriceXType> totalPrice(){
        ArrayList<TotalPriceXType> t = new ArrayList<TotalPriceXType>();

        t.add(new TotalPriceXType("Residential",new BigDecimal("0")));
        t.add(new TotalPriceXType("Multi-Family",new BigDecimal("0")));
        t.add(new TotalPriceXType("Condo",new BigDecimal("0")));
        t.add(new TotalPriceXType("Unknown",new BigDecimal("0")));

        for(Edificio i: elencoPatrimoniale){
            switch(i.getType()){
                case "Residential":
                    t.get(0).add(i.getPrice());
                    break;
                case "Multi-Family":
                    t.get(1).add(i.getPrice());
                    break;
                case "Condo":
                    t.get(2).add(i.getPrice());
                    break;
                case "Unknown":
                    t.get(3).add(i.getPrice());
                    break;
            }
        }
        return t;
    }

    public void remove(Edificio e){
        ArrayList<Edificio> copia = new ArrayList<Edificio>();

        for(Edificio i:elencoPatrimoniale){
            if(!i.equals(e))
                copia.add(i.clone());
        }

        elencoPatrimoniale=copia;
    }


    public void sort(){
        Collections.sort(elencoPatrimoniale);
    }
}