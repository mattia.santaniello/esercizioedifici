import java.math.BigDecimal;

public abstract class Edificio implements Comparable<Edificio>,java.io.Serializable{
   private static final long serialVersionUID = 1L;
   private String street;
   private String city;
   private int zip;
   private String state;
   private int beds;
   private int baths;
   private int sq__ft;
   private String type;
   private String sale_date;
   private BigDecimal price;
   private double latitude;
   private double longitude;
   
   
   public Edificio(String street,String city,int zip,String state,int beds,int baths,int sq__ft,String type,String sale_date,BigDecimal price,double latitude,double longitude){
       this.street=street;
       this.city=city;
       this.zip=zip;
       this.state=state;
       this.beds=beds;
       this.baths=baths;
       this.sq__ft=sq__ft;
       this.type=type;
       this.sale_date=sale_date;
       this.price=price;
       this.latitude=latitude;
       this.longitude=longitude;
   }


   public Edificio(Edificio e){
    this.street=e.getStreet();
    this.city=e.getCity();
    this.zip=e.getZip();
    this.state=e.getState();
    this.beds=e.getBeds();
    this.baths=e.getBaths();
    this.sq__ft=e.getSq__ft();
    this.type=e.getType();
    this.sale_date=e.getSale_date();;
    this.price=e.getPrice();
    this.latitude=e.getLatitude();
    this.longitude=e.getLongitude();
   }

   public abstract Edificio clone();

   public void setStreet(String street){this.street=street;}
   public void setCity(String city){this.city=city;}
   public void setZip(int zip){this.zip=zip;}
   public void setState(String state){this.state=state;}
   public void setBeds(int beds){this.beds=beds;}
   public void setSq__ft(int sq__ft){this.sq__ft=sq__ft;}
   public void setBaths(int baths){this.baths=baths;}
   public void setType(String type){this.type=type;}
   public void setSale_date(String sale_date){this.sale_date=sale_date;}
   public void setPrice(BigDecimal price){this.price=price;}
   public void setLatitude(double latitude){this.latitude=latitude;}
   public void setLongitude(double longitude){this.longitude=longitude;}

   public String getStreet(){ return this.street;}
   public String getCity(){return this.city;}
   public int getZip(){return this.zip;}
   public String getState(){return this.state;}
   public int getBeds(){return this.beds;}
   public int getBaths(){return this.baths;}
   public int getSq__ft(){return this.sq__ft;}
   public String getType(){return this.type;}
   public String getSale_date(){return this.sale_date;}
   public BigDecimal getPrice(){return this.price;}
   public double getLatitude(){return this.latitude;}
   public double getLongitude(){return this.longitude;}

   @Override
   public String toString(){
       String s="street:"+street;

       s+="\ncity:"+city;
       s+="\nzip:"+zip;
       s+="\nstate:"+state;
       s+="\nbeds:"+beds;
       s+= "\nbaths:"+baths;
       s+="\nsq__ft:"+sq__ft;
       s+="\ntype:"+type;
       s+="\nsale_date:"+sale_date;
       s+="\nprice:"+price;
       s+="\nlatitude:"+latitude;
       s+="\nlongitude:"+longitude+"\n";

       return s;
   }

   public boolean equals(Edificio e){
       return e.getCity().matches(city) && e.getStreet().matches(street) && e.getZip() == zip && e.getState().matches(state) && 
              e.getBeds() == beds && e.getBaths() == baths && e.getSq__ft() == sq__ft && e.getType().matches(type)&& e.getSale_date().matches(sale_date) 
              && e.getPrice().doubleValue() == price.doubleValue() && e.getLatitude() == latitude && e.getLongitude() == longitude;
   }

   @Override
   public int compareTo(Edificio e){
       return price.compareTo(e.getPrice());
   }
}

class Condo extends Edificio{
    private static final long serialVersionUID = 1L;

    public Condo(String street, String city, int zip, String state, int beds, int baths, int sq__ft, String type,String sale_date,
            BigDecimal price, double latitude, double longitude) {
        super(street, city, zip, state, beds, baths, sq__ft, type,sale_date, price, latitude, longitude);
    }

    public Condo (Condo c){
        super((Edificio)c);
    }

    @Override
    public Condo clone(){return new Condo(this);}

    public boolean equals(Object o){
        if(o!=null){
            if(o instanceof Condo){
                return super.equals((Edificio)o);
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
}

class MultiFamily extends Edificio{  
    private static final long serialVersionUID = 1L;

    public MultiFamily(String street, String city, int zip, String state, int beds, int baths, int sq__ft, String type,String sale_date,
                        BigDecimal price, double latitude, double longitude) {
            super(street, city, zip, state, beds, baths, sq__ft, type,sale_date, price, latitude, longitude);    }

    public MultiFamily (MultiFamily m){
        super((Edificio)m);
    }

    @Override
    public MultiFamily clone(){return new MultiFamily(this);}

    public boolean equals(Object o){
        if(o!=null){
            if(o instanceof MultiFamily){
                return super.equals((Edificio)o);
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
}

class Residential extends Edificio{
    private static final long serialVersionUID = 1L;

    public Residential(String street, String city, int zip, String state, int beds, int baths, int sq__ft, String type,String sale_date,
                        BigDecimal price, double latitude, double longitude) {
            super(street, city, zip, state, beds, baths, sq__ft, type,sale_date, price, latitude, longitude);    }

    public Residential(Residential r){ super((Edificio)r);}

    @Override
    public Residential clone(){return new Residential(this);}

    public boolean equals(Object o){
        if(o!=null){
            if(o instanceof Residential){
                return super.equals((Edificio)o);
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
}

class Unknown extends Edificio{
    private static final long serialVersionUID = 1L;

    public Unknown(String street, String city, int zip, String state, int beds, int baths, int sq__ft, String type,String sale_date,
                    BigDecimal price, double latitude, double longitude) {
            super(street, city, zip, state, beds, baths, sq__ft, type,sale_date, price, latitude, longitude);    }

    public Unknown(Unknown u){ super((Edificio)u);}

    @Override
    public Unknown clone(){return new Unknown(this);}


    @Override
    public boolean equals(Object o){
        if(o!=null){
            if(o instanceof Unknown){
                return super.equals((Edificio)o);
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
}