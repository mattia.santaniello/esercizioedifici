## Branch Structure

Nel branch "master" si trova l'implementazione della classe Edficio assieme alla classe GestionePatrimoniale,che implementa anche il metodo totalePrice(), che sfrutta la classe TitalePriceXType

Nel branch "versione2" si trova l'implementazioni delle classi presenti nel branch "master" ed anche le classi Punto e CoordinateEdifici
le quali permettono di gestire una serie di punti geografici basati sulla latitudine e la longitudine degli edifici

Nel branch "versione3" troviamo tutte le classi presenti nel branch "versione2" più le classi richieste per questo compito,ossia quelle che
consentono di memorizzare in una lista gli edifici più vicini ad un altro, ad un certo raggio chilometrico